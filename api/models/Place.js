const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const Reviews = new mongoose.Schema({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  quality: {
    type: Number,
    enum: [1, 2, 3, 4, 5]
  },
  service: {
    type: Number,
    enum: [1, 2, 3, 4, 5]
  },
  interior: {
    type: Number,
    enum: [1, 2, 3, 4, 5]
  },
  comment: {
    type: String
  },
});

const Images = new mongoose.Schema({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  url: String
});


const PlaceSchema = new mongoose.Schema({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  title: {
    type: String,
    required: true,
    unique: true,
  },
  description: {
    type: String,
    required: true
  },
  mainImage: {
    type: String,
    required: true
  },
  images: [Images],
  reviews: [Reviews],
  quality: {
    type: Number,
    default: 0,
  },
  service: {
    type: Number,
    default: 0,
  },
  interior: {
    type: Number,
    default: 0,
  },
  overall: {
    type: Number,
    default: 0
  },
});

PlaceSchema.plugin(idValidator);
const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;