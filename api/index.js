const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routers/users/users');
const places = require('./routers/places/places');

const server = express();
server.use(express.json());
server.use(express.static('public'));
server.use(cors());
const port = 8000;

server.use('/users', users);
server.use('/places', places);

const run = async () => {
  await mongoose.connect(config.db.url);

  server.listen(port, () => {
    console.log(`Server is started on ${port} port !`);
  })

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
}
run().catch(e => console.error(e));