const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Place = require("./models/Place");


const run = async () => {
  await mongoose.connect(config.db.url)

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const collection of collections) {
    await mongoose.connection.db.dropCollection(collection.name);
  }

  const [first] = await User.create(
    {
      email: 'user@gmail.com',
      password: '123',
      token: nanoid(),
      name: 'SimpleDummy',
      phone: '+(996)500-50-50-50',
      address: "hell",
    },
    {
      email: 'admin@gmail.com',
      password: '123',
      token: nanoid(),
      name: 'SimpleDummy',
      phone: '+(996)500-50-50-50',
      address: "hell",
      role: 'admin'
    },
  )

  await Place.create(
    {
      creator: first,
      title: 'Summer fest',
      description: 'text',
      mainImage: 'fixtures/ex-portfolio.png'
    },
    {
      creator: first,
      title: 'Lingen',
      description: 'text',
      mainImage: 'fixtures/intent (copy).jpg'
    },
  )
  await mongoose.connection.close();
}
run().catch(console.error)

