const express = require('express');
const Place = require('../../models/Place');
const multer = require("multer");
const config = require("../../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth = require('../../middleware/auth');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});


router.get('/', async (req, res) => {
  try {
    const places = await Place.find().populate();
    res.send(places)
  } catch (e) {
    console.log(e)
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const places = await Place.findById(id).populate({
      path    : 'reviews',
      populate: [
        { path: 'creator' },
      ]
    });

    if (places) {
      res.send(places);
    } else {
      res.status(404).send({message: 'Place not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
})

router.post('/', upload.single('mainImage'), async (req, res) => {
  try {
    const {title, description, checked} = req.body;
    if(checked === 'false') return res.status(400).send({message: 'You have to be agree if you want to allow us share with your data!'})
    if (!title || !description || !req.file) return res.status(400).send({message: 'All fields are required!'});
    const placeData = {
      title,
      description,
      mainImage: 'uploads/' + req.file.filename
    }

    const place = new Place(placeData);
    await place.save();
    res.send(place);
  } catch (e) {

  }
});

router.put('/add/review/:id', auth, async(req, res) => {
  try{
    const {id} = req.params;
    const {quality, service, interior, comment} = req.body;
    const oldPlace = await Place.findById(id);
    if(!oldPlace) return res.status(404).send({message: 'Place not found'});
    const qua = (oldPlace.quality + quality/ (oldPlace.reviews.length + 1))
    const ser = (oldPlace.service + service/ (oldPlace.reviews.length + 1))
    const int = (oldPlace.interior + service/ (oldPlace.reviews.length + 1))

    const overall = ((Number(qua) + Number(ser) +  Number(int)/ 3) + oldPlace.overall) / (oldPlace.reviews.length + 1);
    const place = {
      creator: oldPlace.creator,
      title: oldPlace.title,
      overall: overall.toFixed(1),
      quality: quality.toFixed(1),
      service: service.toFixed(1),
      interior: interior.toFixed(1),
      description: oldPlace.description,
      reviews: [...oldPlace.reviews],
      images: [...oldPlace.images]
    }

    place.reviews.push({creator: req.user, quality, service, interior, comment});
    await Place.findByIdAndUpdate(id, {...place}, {new: true});
    return res.send(place);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.put('/add/images/:id', upload.any('images'), auth, async(req, res) => {
  try{
    const {id} = req.params;
    const oldPlace = await Place.findById(id);
    if(!oldPlace) return res.status(404).send({message: 'Place not found'});
    const place = {
      creator: oldPlace.creator,
      title: oldPlace.title,
      overall: oldPlace.overall,
      quality: oldPlace.quality,
      service: oldPlace.service,
      description: oldPlace.description,
      interior: oldPlace.interior,
      reviews: [...oldPlace.reviews],
      images: [...oldPlace.images]
    }
    req.files.forEach(f => place.images.push({creator: req.user, url: 'uploads/' +  f.filename}));
    await Place.findByIdAndUpdate(id, place, {new: true});
    return res.send(place);
  }catch (e) {
    console.log(e)
    res.sendStatus(500);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const place = await Place.findByIdAndDelete(req.params.id);

    if (place) {
      res.send(`place '${place.title} removed'`);
    } else {
      res.status(404).send({message: 'Place not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/review/:id', async (req, res) => {
  try {
    const place = await Place.find()
    if (place) {
      res.send(`place '${place.title} removed'`);
    } else {
      res.status(404).send({message: 'Place not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});




module.exports = router;