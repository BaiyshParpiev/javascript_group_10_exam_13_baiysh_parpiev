const express = require('express');
const User = require('../../models/User');
const router = express.Router();


router.post('/',  async (req, res) => {
  const preUser = {
    email: req.body.email,
    password: req.body.password,
    name: req.body.name,
    phone: req.body.phone,
    address: req.body.address,
  }


  const user = new User(preUser);
  try {
    user.generateToken();
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
const errMessage = {
  message: 'Пароль или электронная почта неверны'
}
  try {
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(401).send({errors: errMessage});
    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) return res.status(401).send({errors: errMessage});
    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send(user);
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }

});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  if (!token) return res.status(400).send({global:'Вы не вошли в систему'});
  const user = await User.findOne({token});
  if (!user) return res.status(400).send({global: 'Этот пользователь не существует'});
  try {
    user.generateToken();
    await user.save({validateBeforeSave: false});
    return res.send({global: 'Вы успешно вышли из системы'});
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }

});

module.exports = router;