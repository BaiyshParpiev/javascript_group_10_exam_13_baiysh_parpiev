const {I} = inject();

Given(`I have entered in {string} page`, (page) => {
    I.amOnPage(page);
});


Then(`{string} have entered in {string} page`, (user, page) => {
    session(user, () => {
        I.amOnPage(page)
    });
});

Then(`{string} filled out account information and press {string} to confirm registration :`, (user, btnText, table) => {
    session(user, () => {
        table.rows.forEach(row => {
            I.fillField(row.cells[0].value, row.cells[1].value);
            if (row.cells[0].value === 'password') {
                I.fillField('confirmPassword', row.cells[1].value);
            }
        })
        I.click(btnText, '//main//form//button[@type="submit"]');
    })
});

Then(`{string} logs in to the {string} page :`, (user, page, table) => {
    session(user, () => {
        I.amOnPage(page);
        table.rows.forEach(row => {
            I.fillField(row.cells[0].value, row.cells[1].value);
        })
    })
});

Then(`{string} press {string} button`, (user, btnText) => {
    session(user, () => {
        I.click(btnText, '//main//form//button[@type="submit"]');
    })
});
Then(`{string} sees {string}`, (user, message) => {
    session(user, () => {
        I.see(message);
    })
});

Then(`{string} enter login data :`, (user, table) => {
    session(user, () => {
        table.rows.forEach(row => {
            I.fillField(row.cells[0].value, row.cells[1].value);
        })
    })
});



When(`I enter login data :`, (table) => {
    table.rows.forEach(row => {
        I.fillField(row.cells[0].value, row.cells[1].value);
    })
});

When(`I enter registration data :`, (table) => {
    table.rows.forEach(row => {
        I.fillField(row.cells[0].value, row.cells[1].value);
        if (row.cells[0].value === 'password') {
            I.fillField('confirmPassword', row.cells[1].value);
        }
    })
});

When(`I press form button {string}`, (str) => {
    I.click(str, '//main//form//button[@type="submit"]');
});

When(`{string} press form button {string}`, (user, str) => {
    session(user, ()=>{
        I.click(str, '//main//form//button[@type="submit"]');
    })
});

When(`I press button {string}`, (str) => {
    I.click(str);
});

Then(`I wait {int} sec until I see the text {string}`, (sec, msg) => {
    I.waitForText(msg, sec);
});


When(`I press button {string} near :`, (btnText, table) => {
    const cssParent = table.rows[0].cells[0].value;
    const nearValue1 = table.rows[0].cells[1].value;
    const nearValue2 = table.rows[0].cells[2].value;
    I.click(
        `//main//div[contains(@class, "${cssParent}") and contains(., "${nearValue1}") and contains(.,"${nearValue2}")]//button[contains(.,"${btnText}")]`
    );
});

When(`the checkbox {string} is active`, (str) => {
    I.click(str, '//main');
});

When(`I don't see {string} button`, (str) => {
    I.dontSee(str, 'header');
});

When(`{string} don't see {string} button in {string}`, (user, str, context) => {
    session(user, ()=>{
        I.dontSee(str, context);
    })
});

When(`{string} does not see message {string}`, (user, msg) => {
    session(user, ()=>{
        I.dontSee(msg);
    })
});

Then(`I'm on the page {string}`, async (str) => {
    I.waitUrlEquals(str, 2);
});


Then(`I'm not on the {string} page`, async (str) => {
    I.dontSeeInCurrentUrl(str);
});

Then(`{string} not on the {string} page`, async (user, str) => {
    session(user, ()=>{
        I.dontSeeInCurrentUrl(str);
    })
});

Then(`I see message {string}`, async (str) => {
    I.see(str);
});
