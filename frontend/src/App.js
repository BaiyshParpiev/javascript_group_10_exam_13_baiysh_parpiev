import React from 'react';
import Layout from './components/UI/Layout/Layout';
import {Route, Routes} from "react-router-dom";
import AuthPage from "./containers/AuthPage/AuthPage";
import MainPage from "./containers/MainPage/Mainpage";
import Place from "./containers/Place/Place";
import CreatePlace from "./containers/CreatePlace/CreatePlace";


const App = () => {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<MainPage/>}/>
        <Route path='/login' element={<AuthPage/>}/>
        <Route path='/places/:id' element={<Place/>}/>
        <Route path='/create' element={<CreatePlace/>}/>
      </Routes>
    </Layout>
  );
};

export default App;


