import {createTheme} from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: '#4D77E3',
    },
    action1: {
      main: '#122c86',
      contrastText: 'white',
    },
  },
  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          backgroundColor: 'white',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
        },
      },
    },
  },
});

theme.typography.h4 = {
  fontSize: '2.125rem',
  fontWeight: 300,
  [theme.breakpoints.down('sm')] : {
    fontSize: '1.5rem',
  },
}

export default theme;