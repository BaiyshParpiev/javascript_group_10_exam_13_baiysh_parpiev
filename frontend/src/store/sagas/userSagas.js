import {put, takeEvery} from "redux-saga/effects";
import {
  logInUser,
  logInUserFailure,
  logInUserSuccess,
  logOutUser,
  logOutUserFailure,
  logOutUserSuccess,
  registerUser,
  registerUserFailure,
  registerUserSuccess
} from "../actions/userActions";
import {historyReplace} from "../actions/historyActions";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {USERS_LOG_IN_URL, USERS_URL} from "../../config";


export function* registerUserSaga({payload: userData}) {
  try {
    yield axiosApi.post(USERS_URL, userData);
    yield put(registerUserSuccess());
    yield put(historyReplace('/'));
    toast.success('Регистрация успешно!');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(registerUserFailure(err.response?.data));
  }
}
export function* logInUserSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post('/users/sessions', userData);
    yield put(logInUserSuccess(data));
    yield put(historyReplace('/'));
  } catch (err) {
    console.log(err)
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logInUserFailure(err.response?.data));
  }
}
export function* logOutUserSaga({payload: userData}) {
  try {
    yield axiosApi.delete(USERS_LOG_IN_URL, userData);
    yield put(logOutUserSuccess());
    yield put(historyReplace('/'));
    toast.success('Вы успешно вышли из системы');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logOutUserFailure(err.response?.data));
  }
}

const usersSagas = [
  takeEvery(registerUser, registerUserSaga),
  takeEvery(logInUser, logInUserSaga),
  takeEvery(logOutUser, logOutUserSaga),
];

export default usersSagas;