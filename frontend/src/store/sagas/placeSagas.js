import {put, takeEvery} from "redux-saga/effects";
import {
  fetchPlacesRequest,
  fetchPlacesSuccess,
  fetchPlacesFailure,
  fetchPlaceRequest,
  fetchPlaceSuccess,
  fetchPlaceFailure,
  createPlaceRequest,
  createPlaceSuccess,
  createPlaceFailure,
  deletePlaceRequest,
  deletePlaceSuccess,
  deletePlaceFailure,
  addReviewRequest,
  addReviewSuccess,
  addReviewFailure,
  addImageRequest,
  addImageSuccess,
  addImageFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  deleteReviewFailure
} from '../actions/placeActions';
import axiosApi from "../../axiosApi";
import {historyPush, historyReplace} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* fetchPlacesSaga() {
  try {
    const {data} = yield axiosApi.get('/places');
    yield put(fetchPlacesSuccess(data));
    yield put(historyReplace('/'));
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(fetchPlacesFailure(err.response?.data));
  }
}

export function* fetchPlaceSaga({payload: id}) {
  try {
    const {data} = yield axiosApi.get('/places/' + id);
    yield put(fetchPlaceSuccess(data));
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(fetchPlaceFailure(err.response?.data));
  }
}

export function* addImagesSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/places/add/images/' + payload.id, payload.data);
    yield put(addImageSuccess(data));
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(addImageFailure(err.response?.data));
  }
}

export function* addReviewSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/places/add/review/' + payload.id, payload.data);
    yield put(addReviewSuccess(data));
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(addReviewFailure(err.response?.data));
  }
}

export function* createPlaceSaga({payload}) {
  try {
    const {data} = yield axiosApi.post('/places', payload);
    yield put(createPlaceSuccess(data));
    toast.success('Successful created!');
    yield put(historyPush('/'));
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(createPlaceFailure(err.response?.data));
  }
}

export function* deletePlaceSaga({payload}) {
  try {
    yield axiosApi.delete('/places/' + payload);
    yield put(deletePlaceSuccess(payload));
    toast.success('Successful deleted!');
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(deletePlaceFailure(err.response?.data));
  }
}

export function* deleteReviewSaga({payload}) {
  try {
    yield axiosApi.delete('/places/' + payload);
    yield put(deleteReviewSuccess(payload));
    toast.success('Successful deleted!');
  } catch (err) {
    if (err?.response?.data?.message) toast.error(err?.response?.data?.message);
    yield put(deleteReviewFailure(err.response?.data));
  }
}




const placeSagas = [
  takeEvery(fetchPlacesRequest, fetchPlacesSaga),
  takeEvery(fetchPlaceRequest, fetchPlaceSaga),
  takeEvery(addImageRequest, addImagesSaga),
  takeEvery(addReviewRequest, addReviewSaga),
  takeEvery(createPlaceRequest, createPlaceSaga),
  takeEvery(deleteReviewRequest, deleteReviewSaga),
  takeEvery(deletePlaceRequest, deletePlaceSaga),
];

export default placeSagas;