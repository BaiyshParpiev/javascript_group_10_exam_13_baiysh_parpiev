import {all} from 'redux-saga/effects';
import historySagas from "./sagas/historySagas";
import usersSagas from "./sagas/userSagas";
import placeSagas from "./sagas/placeSagas";

export function* rootSagas() {
  yield all([
    ...usersSagas,
    ...historySagas,
    ...placeSagas,
  ])
}