import {createSlice} from "@reduxjs/toolkit";

const name = 'place';

const initialState = {
  places: [],
  place: null,
  fetchPlacesLoading: false,
  fetchPlacesError: null,
  fetchPlaceLoading: false,
  fetchPlaceError: null,
  createPlaceLoading: false,
  createPlaceError: null,
  deletePlaceLoading: false,
  deletePlaceError: null,
  deleteReviewLoading: false,
  deleteReviewError: null,
  addReviewLoading: false,
  addReviewError: null,
  addImageLoading: false,
  addImageError: null,
};

const placeSlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchPlacesRequest(state, action) {
      state.fetchPlacesLoading = true;
    },
    fetchPlacesSuccess(state, action) {
      state.places = action.payload;
      state.fetchPlacesLoading = false;
      state.fetchPlacesError = null
    },
    fetchPlacesFailure(state, action) {
      state.fetchPlacesLoading = false;
      state.fetchPlacesError = action.payload;
    },
    fetchPlaceRequest(state, action) {
      state.fetchPlaceLoading = true;
    },
    fetchPlaceSuccess(state, action) {
      state.place = action.payload;
      state.fetchPlaceLoading = false;
      state.fetchPlaceError = null
    },
    fetchPlaceFailure(state, action) {
      state.fetchPlaceLoading = false;
      state.fetchPlaceError = action.payload;
    },
    createPlaceRequest(state, action) {
      state.createPlaceLoading = true;
    },
    createPlaceSuccess(state, action) {
      state.places = [...state.places, action.payload];
      state.createPlaceLoading = false;
      state.createPlaceError = null
    },
    createPlaceFailure(state, action) {
      state.createPlaceLoading = false;
      state.createPlaceError = action.payload;
    },
    deletePlaceRequest(state, action) {
      state.deletePlaceLoading = true;
    },
    deletePlaceSuccess(state, action) {
      state.places = state.places.filter(p => p._id !== action.payload);
      state.deletePlaceLoading = false;
      state.deletePlaceError = null
    },
    deletePlaceFailure(state, action) {
      state.deletePlaceLoading = false;
      state.deletePlaceError = action.payload;
    },
    deleteReviewRequest(state, action) {
      state.deleteReviewLoading = true;
    },
    deleteReviewSuccess(state, {payload}) {
      state.places = state.places.map(p => p.id === payload.id ? payload : p);
      state.deleteReviewLoading = false;
      state.deleteReviewError = null
    },
    deleteReviewFailure(state, action) {
      state.deleteReviewLoading = false;
      state.deleteReviewError = action.payload;
    },
    addReviewRequest(state, action) {
      state.addReviewLoading = true;
    },
    addReviewSuccess(state, {payload}) {
      state.places = state.places.map(p => p.id === payload.id ? payload : p);
      state.place = payload;
      state.addReviewLoading = false;
      state.addReviewError = null;
    },
    addReviewFailure(state, action) {
      state.addReviewLoading = false;
      state.addReviewError = action.payload;
    },
    addImageRequest(state, action) {
      state.addImageLoading = true;
    },
    addImageSuccess(state, {payload}) {
      state.places = state.places.map(p => p.id === payload.id ? payload : p);
      state.place = payload;
      state.addImageLoading = false;
      state.addImageError = null;
    },
    addImageFailure(state, action) {
      state.addImageLoading = false;
      state.addImageError = action.payload;
    }
  }
});

export default placeSlice;