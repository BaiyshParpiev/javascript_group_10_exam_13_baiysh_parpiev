import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import {rootSagas} from "./rootSagas";
import usersSlice from "./slices/userSlice";
import placeSlice from "./slices/placeSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  places: placeSlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
  preloadedState: persistedState,
});


store.subscribe(() => {
  saveToLocalStorage({
    users: store.getState().users ? {...store.getState().users, profile: store.getState().users?.profile} : null,
  });
})

sagaMiddleware.run(rootSagas);

export default store;