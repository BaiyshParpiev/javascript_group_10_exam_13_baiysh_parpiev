import usersSlice from "../slices/userSlice";

export const {
  registerUser,
  registerUserSuccess,
  registerUserFailure,
  logInUser,
  logInUserSuccess,
  logInUserFailure,
  logOutUser,
  logOutUserSuccess,
  logOutUserFailure,
} = usersSlice.actions;