import placeSlice from "../slices/placeSlice";

export const {
  fetchPlacesRequest,
  fetchPlacesSuccess,
  fetchPlacesFailure,
  fetchPlaceRequest,
  fetchPlaceSuccess,
  fetchPlaceFailure,
  createPlaceRequest,
  createPlaceSuccess,
  createPlaceFailure,
  deletePlaceRequest,
  deletePlaceSuccess,
  deletePlaceFailure,
  addReviewRequest,
  addReviewSuccess,
  addReviewFailure,
  addImageRequest,
  addImageSuccess,
  addImageFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  deleteReviewFailure
} = placeSlice.actions;