export let BASE_URL = 'http://localhost:8000';
export const USERS_URL = '/users';
export const USERS_LOG_IN_URL = '/users/sessions';

if (process.env.REACT_APP_ENV === 'test') {
  BASE_URL = 'http://localhost:8010';
}

if (process.env.NODE_ENV === 'production') {
  BASE_URL = 'https://gdt-bishkek.sytes.net/api';
}