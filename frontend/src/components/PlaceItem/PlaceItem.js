import React from 'react';
import {makeStyles} from "@mui/styles";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Typography
} from "@mui/material";
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import {Link} from "react-router-dom";
import {BASE_URL} from "../../config";
import Rating from '@mui/material/Rating';
const useStyles = makeStyles({
  card: {
    height: '100%'
  },
  media: {
    height: 0,
    paddingTop: '56.25%'
  }
})


const PlaceItem = ({title, mainImage, overall, reviews, images, id, deleteHandler, role}) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={5} md={5} lg={3}>
      <Card className={classes.card}>
        <CardHeader title={title}/>
        <CardMedia
          image={BASE_URL + '/' + mainImage}
          title={title}
          className={classes.media}
        />
        <CardContent>
          <Rating name="read-only" value={overall} readOnly />
          <Typography component="legend">{`${overall},  ${reviews.length} reviews`}</Typography>
          <Typography component="legend">{`${images.length} images`}</Typography>
        </CardContent>
        <CardActions>
          {role === 'admin' && <Button onClick={() => deleteHandler(id)} variant="contained">Delete</Button>}
          <IconButton component={Link} to={'/places/' + id}>
            <ArrowForwardIcon/>
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default PlaceItem;