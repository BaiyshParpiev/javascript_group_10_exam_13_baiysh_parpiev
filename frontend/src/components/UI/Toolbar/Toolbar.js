import React from 'react';
import NavigationItem from "../NavigationItem/NavigationItem";
import c from './Toolbar.module.css';
import {useSelector, useDispatch} from "react-redux";
import {Button, Grid} from "@mui/material";
import {logOutUser} from "../../../store/actions/userActions";

const Toolbar = () => {
  const dispatch = useDispatch();
  const {profile} = useSelector(state => state.users);
  const logoutHandler = () => {
    dispatch(logOutUser());
  }
  return (
    <header>
      <div className={c.Toolbar}>
        <div className={`${c.header} container`}>
          <div className={c.header__logo}>
            <NavigationItem to="/">Places</NavigationItem>
          </div>
          <div className={c.header__nav}>
            {!profile ? (
              <NavigationItem to="/login">Login</NavigationItem>
            ): (
                <Button
                  type="submit"
                  variant="contained"
                  color={'action1'}
                  style={{marginBottom: "20px"}}
                  onClick={logoutHandler}
                >
                  Logout
                </Button>
            )}
            {profile && <NavigationItem to="/create">Create Place</NavigationItem>}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Toolbar;