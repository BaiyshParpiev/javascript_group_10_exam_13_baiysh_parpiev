import React, {useState} from 'react';
import {Button, Checkbox, FormControlLabel, Grid, TextField, Typography} from "@mui/material";
import {useDispatch} from "react-redux";
import {createPlaceRequest} from "../../store/actions/placeActions";

const CreatePlace = () => {
  const dispatch = useDispatch();
  const [checked, setChecked] = useState(false);
  const [state, setState] = useState({
    title: "",
    description: "",
    mainImage: null,
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('checked', checked);

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    dispatch(createPlaceRequest(formData));

  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file};
    });
  };


  return (
    <Grid container item xs={11} sm={10} sx={{margin: "20px auto"}} justifyContent="center" alignItems="center"
          direction="column">
      <Grid item xs={12} mt={3} mb={3}>
        <Typography textAlign="center" variant="h4">Creating new Place</Typography>
      </Grid>
      <Grid container item spacing={3} xs={12} direction="column" component="form" onSubmit={submitFormHandler}>
        <Grid item xs={12}>
          <TextField
            name="title"
            label="Title"
            fullWidth
            variant="outlined"
            color="primary"
            value={state.title}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="description"
            label="Description"
            fullWidth
            variant="outlined"
            color="primary"
            multiline
            rows={4}
            value={state.description}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={"file"}
            name={"mainImage"}
            fullWidth
            onChange={fileChangeHandler}
          />
        </Grid>
        <Grid item xs={12} sm={"auto"}>
          <FormControlLabel
            control={
              <Checkbox
                onClick={() => {
                  setChecked(prevState => !prevState)
                }}
                checked={checked}
                color="success"
              />}
            label={<Typography variant={"body1"}>I agree</Typography>}
            labelPlacement={'start'}
          />
        </Grid>

        <Grid item xs={12}>
          <Button
            type="submit"
            variant="contained"
            color={'action1'}
            fullWidth
          >
            Create
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CreatePlace;
