import React, {useEffect, useState} from 'react';
import {Container, Grid, Link} from "@mui/material";
import RegisterForm from "./RegisterForm/RegisterForm";
import LoginForm from "./LoginForm/LoginForm";
import {useDispatch, useSelector} from "react-redux";
import {logInUser, logInUserFailure, registerUser, registerUserFailure} from "../../store/actions/userActions";

const AuthPage = () => {
  const [isRegistered, setIsRegistered] = useState(true);
  const dispatch = useDispatch();
  const errSignUp = useSelector(state => state.users.errSignUp);
  const errLogIn = useSelector(state => state.users.errLogIn);
  const signUpLoading = useSelector(state => state.users.signUpLoading);
  const logInLoading = useSelector(state => state.users.logInLoading);

  const registrationSubmitHandler = (data) => {
    dispatch(registerUser(data));
  };

  const registerHandle = (e) => {
    e.preventDefault()
    setIsRegistered(prevState => !prevState)
  };
  const loginSubmitHandler = (data) => {
    dispatch(logInUser(data));
  };

  useEffect(()=>{
    if (errSignUp) {
      dispatch(registerUserFailure(null));
    }
    if (errLogIn) {
      dispatch(logInUserFailure(null));
    }
  }, [isRegistered])


  return (
    <Container sx={{padding: "20px 0",}}>
      {!isRegistered && (
        <RegisterForm
          handleSubmit={registrationSubmitHandler}
          errSignUp={errSignUp}
          loadingSubmit={signUpLoading}
        />
      )}
      {isRegistered && (
        <LoginForm
          errLogIn={errLogIn?.errors?.message}
          handleSubmit={loginSubmitHandler}
          submitLoading={logInLoading}
        />
      )
      }

      <Grid container textAlign={"center"} justifyContent={"center"}>
        <Grid item>
          <Link onClick={registerHandle}
                textAlign={"center"}
                sx={{":hover": {cursor: "pointer"}}}
          >
            {isRegistered ? 'Еще нет аккаунта? Зарегистрируйтесь.' : 'Есть аккаунт? Выполните вход.'}
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AuthPage;

