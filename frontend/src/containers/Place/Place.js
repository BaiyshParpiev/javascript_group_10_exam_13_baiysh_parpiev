import React, {useEffect, useState} from 'react';
import {Button, Grid, MenuItem, TextField, Typography} from "@mui/material";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addImageRequest, addReviewRequest, fetchPlaceRequest, deleteReviewRequest} from "../../store/actions/placeActions";
import {BASE_URL} from "../../config";
import Rating from "@mui/material/Rating";
import FileInput from "../../components/FileInput/FileInput";



const Place = () => {
  const {id} = useParams();
  const dispatch = useDispatch();
  const {place} = useSelector(state => state.places);
  const {profile} = useSelector(state => state.users);
  const [images, setImages] = useState([]);
  const [rating, setRating] = useState({
    quality: '',
    service: '',
    interior: '',
    comment: '',
  });

  let changeAble = {
    quality: 0,
    service: 0,
    interior: 0,
    overall: 0,
  }

  if(place){
    changeAble = {
      quality: place?.quality,
      service: place?.service,
      interior: place?.interior,
      overall: place?.overall,
    }
  }


  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setRating(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const onSubmitReviewHandler = e => {
    e.preventDefault();
    dispatch(addReviewRequest({data: rating, id}))
    setRating({
      quality: '',
      service: '',
      interior: '',
      comment: '',
    })
  }


  useEffect(() => {
    dispatch(fetchPlaceRequest(id))
  }, [dispatch]);

  const uploadFileHandler = (event) => {
    setImages(event.target.files);
  };

  const submitImages = (e) => {
    e.preventDefault();
    const formData = new FormData();

    for(let i = 0; i < images.length; i++){
      formData.append('images', images[i]);
    }

    dispatch(addImageRequest({data: formData, id}));
  };

  const reviewHandler = (id) => {
    dispatch(deleteReviewRequest(id))
  }


  return (
    <Grid container flexDirection="column" spacing={3} style={{padding: '10px 20px'}}>
      <Grid container item xs={12}>
        <Grid container item xs={12} md={6}>
          <Grid item xs={12}><Typography variant="h3">{place?.title}</Typography></Grid>
          <Typography variant="body1">{place?.description}</Typography>
        </Grid>
        <Grid item xs={12} md={6}>
          {place?.mainImage && <img style={{width: '50%'}} src={BASE_URL + '/' + place?.mainImage} alt={place?.title}/>}
        </Grid>
      </Grid>
      <Grid container item xs={12}>
        <Typography variant="h4">Gallery</Typography>
        <Grid container item justifyContent="flex-start" xs={12}>
          {place?.images.map((p, i) => (
            <Grid key={i} item xs={6} md={1}>
              <img style={{width: '90px'}} src={BASE_URL + '/' + p.url} alt={p.url}/>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid container item xs={12} justifyContent="center" alignItems="center">
        <Typography variant="h4">Rating</Typography>
        <Grid item xs={12}>
          <Typography variant="body1">Overall: </Typography>
          <Rating name="read-only" defaultValue={0} value={changeAble?.overall} />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="body1">Quality of food</Typography>
          <Rating name="read-only" defaultValue={0} value={changeAble?.quality} readOnly />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="body1">Service quality: </Typography>
          <Rating name="read-only" defaultValue={0} value={changeAble?.service} readOnly />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="body1">Interior </Typography>
          <Rating name="read-only" defaultValue={0} value={changeAble?.interior} readOnly />
        </Grid>
      </Grid>
      <Grid container item xs={12} justifyContent="center" alignItems="center">
        <Typography variant="h4" mt={5}>Reviews</Typography>
        {place?.reviews.map((p, i) => (
          <Grid key={i} container item xs={12} style={{border: '2px solid black', borderRadius: '4px'}}>
            <Typography variant="body1">{p.comment}. Author: {p?.creator?.name}</Typography>
            <Grid item xs={12}>
              <Typography variant="body1">Quality of food</Typography>
              <Rating name="read-only" defaultValue={0} value={p?.quality} readOnly />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body1">Service quality: </Typography>
              <Rating name="read-only" defaultValue={0} value={p?.service} readOnly />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body1">Interior </Typography>
              <Rating name="read-only" defaultValue={0} value={p?.interior} readOnly />
            </Grid>
            <Grid item xs={5} md={7} pl={1}>
              <Button
                fullWidth
                color={'action1'}
                variant="contained"
                sx={{padding: '10px 0'}}
                onClick={() => reviewHandler(p?._id)}
              >
                Delete review
              </Button>
            </Grid>
          </Grid>
        ))}
      </Grid>
      {!place?.reviews.filter(p => profile?._id === p.creator).length > 0 && (
        <>
          <Grid container item xs={12} justifyContent="center" alignItems="center">
            <Typography variant="h3">Add review</Typography>
            <TextField
              fullWidth
              multiline
              rows={4}
              color={'primary'}
              type="text"
              label="Комментарий"
              name="comment"
              value={rating.comment}
              onChange={inputChangeHandler}
            />
          </Grid>
          <Grid container item xs={12} justifyContent="space-between" alignItems="center">
            <Grid item xs={6} md={3}>
              <TextField
                select
                label="Quality"
                fullWidth
                color={'action1'}
                onChange={inputChangeHandler}
                name="quality"
                value={rating.quality}
              >
                {[1, 2, 3, 4, 5].map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={6} md={3}>
              <TextField
                select
                label="Service"
                fullWidth
                color={'action1'}
                onChange={inputChangeHandler}
                name="service"
                value={rating.service}
              >
                {[1, 2, 3, 4, 5].map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={6} md={3} mt={2}>
              <TextField
                select
                label="Interior"
                fullWidth
                color={'action1'}
                onChange={inputChangeHandler}
                name="interior"
                value={rating.interior}
              >
                {[1, 2, 3, 4, 5].map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item mt={2} xs={6} md={3}>
              <Button onClick={onSubmitReviewHandler} variant="contained" color="primary">Submit review</Button>
            </Grid>
          </Grid>
      </>)}
      <Grid container item xs={12} justifyContent="space-between" alignItems="center">
        <Grid item xs={12} md={7} pl={1}>
          <FileInput
            label={'Фото'}
            name="images"
            value={images}
            onChange={uploadFileHandler}
          />
        </Grid>
        <Grid item xs={12} md={7} pl={1}>
          <Button
            fullWidth
            color={'action1'}
            variant="contained"
            sx={{padding: '10px 0'}}
            onClick={submitImages}
          >
            Upload Images
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Place;