import React, {useEffect} from 'react';
import {Grid, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {deletePlaceRequest, fetchPlacesRequest} from "../../store/actions/placeActions";
import PlaceItem from '../../components/PlaceItem/PlaceItem';

const Mainpage = () => {
  const dispatch = useDispatch();
  const {places} = useSelector(state => state.places);
  const {profile} = useSelector(state => state.users);

  const deleteHandler = (id) => {
    dispatch(deletePlaceRequest(id))
  }

  useEffect(() => {
    dispatch(fetchPlacesRequest());
  }, [dispatch]);
  return (
    <Grid container flexDirection="column">
      <Typography variant="h2" textAlign="center">All places</Typography>
      <Grid container item xs={12} p={2} spacing={2}>
        {places.map((p) => (
          <PlaceItem
            key={p._id}
            id={p._id}
            title={p.title}
            overall={p.overall}
            images={p.images}
            reviews={p.reviews}
            mainImage={p.mainImage}
            role={profile?.role}
            deleteHandler={deleteHandler}
          />
        ))}
      </Grid>
    </Grid>
  );
};

export default Mainpage;